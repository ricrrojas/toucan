import { createSelector } from 'reselect'
import moment from 'moment'
import { getLastWeek } from '../utils/DateUtils'

const getTasks = (state, filter) => ({
  tasks : state.tasksReducer.getIn(['tasks']).toList(),
  filter,
  isFetching : state.tasksReducer.getIn(['isPopulating'])
})

const getTasksCompleted = (state ) => ({
  tasks : state.tasksReducer.getIn(['tasks']).toList().filter(t => t.completed)
})


export const tasksSelector  = createSelector(
  [ getTasks ],
  ({ tasks , filter , isFetching}) => {
    const lastWeek = getLastWeek()
    const taskByDate = tasks.filter(task => task.completed && task.created > lastWeek)
        .reduce((prev,curr) => {
          const date = moment(curr.created).format('YYYY-MM-DD')
          if(!prev[date]) {
            prev[date] = { date, corta : 0, larga: 0, media : 0 }
          }
          const type = curr.getType()
          prev[date][type] = prev[date][type] + 1
          return prev
    }, { })

    return {
      tasks : tasks.filter(task => !task.completed && (filter  ? task.getType() === filter : true ) ),
      completedTask : Object.values(taskByDate),
      isFetching
    }
  }
)




export const completedTasksSelector  = createSelector(
  [ getTasksCompleted ],
  ({ tasks  }) => {
    return { tasks }
  }
)
