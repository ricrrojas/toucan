import React from 'react'
import { Provider } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { ConnectedRouter  } from 'react-router-redux'
import Routes from '../routes'
import AppBarHeader from '../routes/AppBar'


const App =  ({ store,history }) =>
  (
    <div style={{ backgroundColor : '#e5e5e5', height:'100vh', width: '100vw' }}>
      <MuiThemeProvider>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <div style={{ height: '100%', width :'100%', display: 'flex',flexDirection : 'column' }}>
              <AppBarHeader  />
                <div style={{ display: 'flex', justifyContent : 'center', overflow:'scroll',flex: 1 }}>
                  <div>
                    <Routes />
                  </div>
                </div>
              </div>
           </ConnectedRouter>
         </Provider>
      </MuiThemeProvider>
  </div>
)

export default App
