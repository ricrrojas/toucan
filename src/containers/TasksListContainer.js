import React from 'react'
import FilterTask from '../components/Tareas/FilterTask'
import ListTask from '../components/Tareas/ListTask'



class TasksListsContainer extends React.Component {

  state = {
    filter  : null
  }

  onChange = (value) => {
      this.setState({
        filter: value
      })
  }


  render() {
      return  (
        <div>
          <FilterTask onChange={this.onChange} />
          <ListTask filter={this.state.filter} />
        </div>
      )
  }
}


export default TasksListsContainer
