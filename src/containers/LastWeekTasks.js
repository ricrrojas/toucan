import React, { Component } from 'react';
import { connect } from 'react-redux'
import { tasksSelector }  from '../selector/Tasks'
import GraphTask from '../components/Tareas/GraphTask'


const mapStateToProps = (state, props) => ({
    tasks : tasksSelector(state, props.filter)
})


class LastWeekTasks extends Component {

  render() {
    const {
      tasks
    } = this.props

    const data = tasks.completedTask
    const dates = data.map(d => d.date)

    return (
      <div style={{ maxWidth :'500px' }}>
          <GraphTask data={data} dates={dates} />
      </div>
    )
  }
}


LastWeekTasks.defaultProps = {
  tasks : [],
}

export default connect(mapStateToProps,null)(LastWeekTasks)
