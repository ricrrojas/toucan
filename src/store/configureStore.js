import {
  createStore,
  applyMiddleware,
  compose,
} from 'redux';
import {
  persistStore,
} from 'redux-persist'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import rootReducer from '../reducers'
import immutableToJS from '../utils/immutableToJS'
import rootSaga from '../sagas'
import {  routerMiddleware  } from 'react-router-redux'

const dev = process.env.NODE_ENV === 'development'

const logger = createLogger({
  collapsed: true,
  stateTransformer : immutableToJS,
})

const configureStore = (history) => {
    const sagaMiddleware = createSagaMiddleware()
    const middleware = [sagaMiddleware,thunk, routerMiddleware(history)]
    if(dev) {
      middleware.push(logger)
    }
    const enhancers = [applyMiddleware(...middleware)]
    if(dev){
        enhancers.push(window.devToolsExtension ? window.devToolsExtension() : f => f )
    }
    const store = createStore(rootReducer, compose(...enhancers))
    rootSaga.forEach(s => sagaMiddleware.run(s))
    const persistor = persistStore(store)
    return store
}

export default configureStore
