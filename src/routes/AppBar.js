import React from 'react'
import AppBar from 'material-ui/AppBar'
import ButtonRouter from '../components/Buttons/ButtonRouter'

const AppBarHeader = () => (
  <AppBar title="Toucan">
    <div>
      <ButtonRouter to='/' label='Inicio'/>
      <ButtonRouter to='/tasks' label='Tareas Completadas'/>
      <ButtonRouter to='/lastweek' label='Ver mi ultima semana'/>
      <ButtonRouter to='/task' label='Agregar Tarea'/>
    </div>
  </AppBar>
)


export default AppBarHeader
