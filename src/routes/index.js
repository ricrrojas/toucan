import React from 'react'
import styles from '../sytles.css'
import { Route } from 'react-router-dom'

import TaskList from '../containers/TaskList'
import LastWeekTasks from '../containers/LastWeekTasks'
import CreateTask from '../components/Tareas/CreateTask'
import EditTask from '../components/Tareas/EditTask'
import CompletedTasks from '../components/Tareas/CompletedTasks'


const Routes = () => (
  <div className={styles.card}>
    <Route exact path="/" component={TaskList} />
    <Route exact path="/lastweek" component={LastWeekTasks}/>
    <Route exact path="/task" component={CreateTask}/>
    <Route exact path="/task/:id" component={EditTask}/>
    <Route exact path="/tasks" component={CompletedTasks}/>
  </div>
)

export default Routes
