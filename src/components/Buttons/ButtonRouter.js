import React from 'react'
import { Route } from 'react-router-dom'
import RaisedButton from 'material-ui/RaisedButton'

const style = {
  margin: 12,
}

const RaisedButtonRouter =({ to, label}) => (
    <Route render={({ history}) => (
      <RaisedButton
            label={label}
            onClick={() => { history.push(to)}}
            primary={true} style={style} />
          )} />
)


RaisedButtonRouter.defaultProps ={
  to: '',
  label: ''
}

export default RaisedButtonRouter
