import React from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import { Task } from '../../reducers/Tasks'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton'


const style = {
  margin: 12,
}

class FormTask extends React.Component{


  constructor(props){
     super(props)
     this.state = {
       form : props.task.toJS(),
       type: 1,
       errors: { }
     }
  }

  validate = () => {
     const errors = {
        duration  : this.state.form.duration > 0 && this.state.form.duration <= 120  ? '': 'Duración inválida',
        description : (this.state.form.description && this.state.form.description.length !== 0)  ? '': 'Descripción inválida'
     }
     this.setState({ errors })
  }

  onChangeSelect = (event,key,value) => {
    this.setState({
          form : {
              ...this.state.form,
              'duration' : value
          }
    },this.validate)

  }

  onChange = (event,newValue) => {
    this.setState({
          form : {
              ...this.state.form,
              [event.target.name] : newValue
          }
    },this.validate)
 }

chageDurationType = (event,value) => {
      this.setState({
        type : value
    })
}

 onSubmit = () => {
    this.props.onSubmit(this.state.form)
 }

 render(){
   const  {
     submitText
   } = this.props
   return  (
      <div>
        <TextField  name="description"
                    floatingLabelText="Descripción"
                    onChange={this.onChange}
                    value={this.state.form.description}
                    errorText={this.state.errors.description} />
                    <br />

          <RadioButtonGroup  name="durationType" valueSelected={this.state.type} onChange={this.chageDurationType} >
                  <RadioButton
                    value={1}
                    label="Duraciones Prestablecida" />
                  <RadioButton
                    value={2}
                    label="Valor Personalizado" />
                </RadioButtonGroup>

          {this.state.type === 2 &&
            (
              <div>
                  <TextField name="duration"
                    floatingLabelText="Duración"
                    onChange={this.onChange}
                    value={this.state.form.duration}
                    errorText={this.state.errors.duration} />
                </div>
              )
          }
          {this.state.type === 1 &&
            (
              <SelectField
                      name='duration'
                      floatingLabelText="Duración"
                      value={this.state.form.duration}
                      onChange={this.onChangeSelect}
                      errorText={this.state.errors.duration} >
                      <MenuItem value={30} primaryText="Corta (30 min)" />
                      <MenuItem value={45} primaryText="Media (45 min)" />
                      <MenuItem value={60} primaryText="Larga (60 min)" />
             </SelectField>
           )
         }
         <br />
        <RaisedButton label={submitText} primary={true} style={style}  onClick={this.onSubmit}/>
      </div>
    )
  }
}



FormTask.defaultProps = {
  task :  new Task() ,
  submitText : 'Guardar',
  onSubmit : () => { },
}

export default FormTask
