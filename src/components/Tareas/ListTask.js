import React from 'react'
import TaskCard from './TaskCard'
import { connect } from 'react-redux'
import { tasksSelector }  from '../../selector/Tasks'
import { bindActionCreators } from 'redux'
import { actions as tasksActions } from '../../reducers/Tasks'
import { startTask, stopTask} from '../../sagas/Tasks'
import ControlTask from './ControlTasks'
import RaisedButton from 'material-ui/RaisedButton'
import LoadingOverlay from '../LoadingOverlay'

const mapStateToProps = (state, props) => ({
    tasks : tasksSelector(state, props.filter)
})


const mapDispatchToProps = dispatch => ({
  actions : bindActionCreators(Object.assign({}, tasksActions, { startTask, stopTask }), dispatch)
})

const style = {
  margin: 12,
}


class ListTask extends React.Component {

  componentDidMount(){
    this.props.actions.getTasks()
  }


  render() {
    const {
      tasks,
      actions,
    } = this.props

    return (
        <div>
          <h3>Tareas por hacer</h3>
          <ControlTask tasks={tasks.tasks} {...actions} />
          <LoadingOverlay isFetching={tasks.isFetching}>
            <RaisedButton label="Poblar Tareas"  onClick={() => actions.populateTasks()}  style={style}/>
          </LoadingOverlay>
          { tasks.tasks.length === 0 && <div> No exiten tareas</div> }
          { tasks.tasks.map((task, idx) => ( <TaskCard key={idx} task={task}  {...actions} /> ) ) }
       </div>
    )
  }
}


ListTask.defaultProps = {
  tasks : [],
}

export default connect(mapStateToProps,mapDispatchToProps)(ListTask)
