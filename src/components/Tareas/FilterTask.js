import React from 'react'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


class FilterTask extends  React.Component{

    state = {
      value :  null
    }

    handleChange = (event, index, value) => {
        this.props.onChange(value)
        this.setState({ value })
    }

    render(){
      return (
        <SelectField
            floatingLabelText="Duración"
            value={this.state.value}
            onChange={this.handleChange} >
            <MenuItem value={null} primaryText="Todos" />
            <MenuItem value={'corta'} primaryText="Corta" />
            <MenuItem value={'media'} primaryText="Media" />
            <MenuItem value={'larga'} primaryText="Larga" />
        </SelectField>
      )
    }
}

FilterTask.defaultProps = {
    onChange : () => {}
}

export default FilterTask
