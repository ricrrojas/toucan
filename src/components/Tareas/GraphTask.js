import React from 'react'
import { VictoryBar,VictoryChart,VictoryTheme,VictoryGroup,VictoryAxis,VictoryLegend } from 'victory'
import moment from 'moment'



const GraphTask =  ({ data, dates }) => {

  return (
    <VictoryChart
     theme={VictoryTheme.material}
      height={500} width={500} >
      <VictoryLegend x={125} y={50}
         title="Tareas Completadas"
         centerTitle
         orientation="horizontal"
         gutter={20}
         style={{ border: { stroke: "black" }, title: {fontSize: 20 } }}
         data={[
           { name: "Corta", symbol: { fill: "tomato" } },
           { name: "Media", symbol: { fill: "orange" } },
           { name: "Larga", symbol: { fill: "gold" } }
         ]}
       />

      <VictoryGroup
          scale={{ x: "time" }}
          offset={25}
          colorScale={["tomato", "orange", "gold"]} >
        <VictoryBar data={data} x='date' y='corta' />
        <VictoryBar data={data} x='date' y='media' />
        <VictoryBar data={data} x='date' y='larga' />
      </VictoryGroup>

     <VictoryAxis
      label="Fecha"
      style={{
        axisLabel: { padding: 30 }
      }}
      tickValues={dates}
      tickFormat={(t) => `${moment(t).format('DD-MM')}`}
    />
    <VictoryAxis dependentAxis
      label="No Tareas"
      tickFormat={(t) => `${t}`}
      style={{
        axisLabel: { padding: 40 }
      }}
    />
   </VictoryChart>
  )
}

GraphTask.defaultProps = {
  data: [],
  dates : []
}

export default GraphTask
