import React from 'react'
import TaskCard from './TaskCard'
import { connect } from 'react-redux'
import { completedTasksSelector }  from '../../selector/Tasks'
import { bindActionCreators } from 'redux'
import { actions as tasksActions } from '../../reducers/Tasks'
import { startTask, stopTask} from '../../sagas/Tasks'

const mapStateToProps = (state ) => ({
  tasks : completedTasksSelector(state)
})

const mapDispatchToProps = dispatch => ({
  actions : bindActionCreators(Object.assign({}, tasksActions, { startTask, stopTask }), dispatch)
})


class CompletedTask extends React.Component {


  render() {
    const {
      tasks,
      actions,
    } = this.props

    return (
        <div>
          <h3>Tareas completadas</h3>
          { tasks.tasks.length === 0 && <div> No exiten tareas completadas</div> }
          { tasks.tasks.map((task, idx) => ( <TaskCard key={idx} task={task}  {...actions} /> ) ) }
       </div>
    )
  }
}


CompletedTask.defaultProps = {
  tasks : [],
}

export default connect(mapStateToProps,mapDispatchToProps)(CompletedTask)
