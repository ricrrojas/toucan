import React  from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import FormTask from './FormTask'
import {  actions as tasksActions  } from '../../reducers/Tasks'


const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(Object.assign({}, tasksActions),  dispatch)
  }
}


class CreateTask extends React.Component {
  
  onCreate = (task) => {
      this.props.actions.createTask(task)
  }

  render() {
      return (
        <div>
          <h3>Crear Tarea</h3>
          <FormTask  onSubmit={this.onCreate}  />
        </div>
      )
  }

}

export default connect(null, mapDispatchToProps)(CreateTask)
