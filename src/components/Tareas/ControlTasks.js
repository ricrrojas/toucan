import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import LoadingOverlay from '../LoadingOverlay'
import { convertToMinutes } from '../../utils/TaskUtils'


const style = {
  margin: 12,
}


const ControlTask = ({ tasks,  startTask, completeTask, stopTask, restartTask , populateTasks }) => {
    const task =  tasks.first()

    if(!task) {
        return (
          <div>
              <label>No hay tarea pendientes</label>
          </div>
        )
    }

    const duration =  convertToMinutes(task.duration)
    const progress =  convertToMinutes(task.progress)

    return (
      <LoadingOverlay isFetching={task.isFetching}>
        <div>
            <div>
                Tarea en curso
            </div>
            <div>
                Progreso :  {`${progress.minutes}:${progress.seconds}`} <br/>
                Completada : {task.completed ? 'Si': 'No'}<br/>
                Duración: {`${duration.minutes}:${duration.seconds}`} <br/>
            </div>
            { task.started ?
                  <RaisedButton label="Stop" onClick={() => stopTask(task)} primary={true} style={style}/>
                : <RaisedButton label="Comenzar"  onClick={() => startTask(task)}  primary={true} style={style}/>
            }
            <RaisedButton label="Reiniciar" onClick={() => restartTask(task)}  secondary={true} style={style}/>
            <RaisedButton label="Completar"  onClick={() => completeTask(task)}  style={style}/>
        </div>
        </LoadingOverlay>
    )
}

ControlTask.defaultProps = {
    tasks: [],
    startTask : () => {},
    stopTask : () => {},
    completeTask : () => {},
    restartTask:  () => {},
}

export  default ControlTask
