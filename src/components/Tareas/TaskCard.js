import React from 'react'
import PropTypes from 'prop-types'
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import { Task } from '../../reducers/Tasks'
import moment from 'moment'
import { convertToMinutes } from '../../utils/TaskUtils'
import RaisedButtonRouter from '../Buttons/ButtonRouter'
import LoadingOverlay from '../LoadingOverlay'

const style = {
  margin: 12,
}


const TaskCard = ({ task, completeTask, onEdit, deleteTask,startTask , stopTask}) => {
    const duration =  convertToMinutes(task.duration)
    const progress =  convertToMinutes(task.progress)

    return (
        <Card>
            <CardTitle>
                Descripción: {task.description}
            </CardTitle>
            <CardText>
                Progreso :  {`${progress.minutes}:${progress.seconds}`} <br/>
                Completada : {task.completed ? 'Si': 'No'}<br/>
                Duración: {`${duration.minutes}:${duration.seconds}`} <br/>
                Fecha Creacion: {moment(task.created).format('MMMM Do YYYY, h:mm:ss a')}<br/>
                Tipo tarea: {task.getType()}
            </CardText>

            <CardActions>
              <LoadingOverlay isFetching={task.isFetching}>
                <RaisedButtonRouter to={`/task/${task.id}`} label='Editar' />
                <RaisedButton label="Borrar" onClick={() => deleteTask(task)} secondary={true} style={style} />
              </LoadingOverlay>
            </CardActions>
        </Card>
    )
}


TaskCard.defaultProps = {
    task : new Task(),
    completeTask : () => { },
    onEdit : () => { },
    startTask : () => { },
    stopTask :  () => { },
    deleteTask: () => { },
}

TaskCard.propTypes = {
  completeTask: PropTypes.func,
  onEdit: PropTypes.func,
  startTask: PropTypes.func,
  deleteTask: PropTypes.func,
}

export default TaskCard
