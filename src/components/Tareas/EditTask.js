import React, { Component } from 'react'
import FormTask from './FormTask'
import { connect } from 'react-redux'


const mapStateToProps = (state, props) => ({
    task : state.tasksReducer.getIn(['tasks', props.match.params.id])
})




class EditTask extends Component {

  render() {
      const { task } = this.props
      return (
        <div>
          <h3>Editar Tarea</h3>
          <FormTask submitText='Guardar' task={task}/>
        </div>
      )
  }
}


export default connect(mapStateToProps, null)(EditTask)
