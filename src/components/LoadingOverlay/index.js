import React from 'react'
import styles from './styles.css'

class LoadingOverlay extends React.Component {

      render () {
        return (
          <div style={{position :'relative' }}>
            {this.props.children}
            {
              this.props.isFetching &&
              (<div className={styles.overlay}>
                <div className={styles.loader} />
              </div>)
            }
          </div>
        )
    }
}


LoadingOverlay.defaultProps = {
  isFetching : false,
}


export default LoadingOverlay
