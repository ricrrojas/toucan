import React from 'react'
import styles from './styles.css'


const Spinner = ({ text , customStyles}) => (
  <div className={styles.loader} styles={customStyles}>{text}</div>
)

Spinner.defaultProps = {
  text: 'Cargando...',
  customStyles : {}
}


export default Spinner
