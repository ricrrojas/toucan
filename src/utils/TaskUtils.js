import { getLastWeek } from './DateUtils'
import { Task } from '../reducers/Tasks'

export const convertToSecons = (minutes, seconds) => (minutes * 60) + seconds

export const convertToMinutes = (_seconds) => {
    const seconds = _seconds % 60
    const minutes = (_seconds - seconds) / 60
    return { minutes, seconds }
}


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const createRandomTask = (description) =>{
  const lastWeek = getLastWeek()
  const created = lastWeek.setDate(lastWeek.getDate() + getRandomInt(0,7))
  const duration =  getRandomInt(30, 120)
  const progress =  getRandomInt(Math.floor(duration * 0.80), duration)
  const task = new Task({ description, created ,duration, progress})
  if(progress === duration){
      task.set('completed', true).set('completeDate', created)
  }
  return task
}
