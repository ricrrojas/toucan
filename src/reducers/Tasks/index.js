import  { Record ,OrderedMap, fromJS } from 'immutable'

export const types = {
    TASK_ADD : 'tasks/TASK_ADD',
    TASK_ADD_FAILURE: 'tasks/TASK_ADD_FAILURE',
    TASK_ADD_SUCCESS : 'tasks/TASK_ADD_SUCCESS',

    TASK_REMOVE : 'tasks/TASK_REMOVE',
    TASK_REMOVE_SUCCESS : 'tasks/TASK_REMOVE_SUCCESS',
    TASK_REMOVE_FAILURE : 'tasks/TASK_REMOVE_FAILURE',


    TASK_GET_ALL_SUCCESS : 'tasks/TASK_GET_ALL_SUCCESS',
    TASK_GET_ALL_REQUEST : 'tasks/TASK_GET_ALL_REQUEST',
    TASK_GET_ALL_FAILURE : 'tasks/TASK_GET_ALL_FAILURE',

    TASK_COMPLETE : 'tasks/TASK_COMPLETE',
    TASK_COMPLETE_SUCCESS : 'tasks/TASK_COMPLETE_SUCCESS',
    TASK_COMPLETE_FAILURE : 'tasks/TASK_COMPLETE_FAILURE',

    TASK_UPDATE : 'tasks/TASK_UPDATE',
    TASK_UPDATE_SUCCESS : 'tasks/TASK_UPDATE_SUCCESS',
    TASK_UPDATE_FAILURE : 'tasks/TASK_UPDATE_FAILURE',


    TASK_START : 'tasks/TASK_START',
    TASK_TICK : 'tasks/TASK_TICK',
    TASK_STOP : 'tasks/TASK_STOP',
    TASK_RESTART : 'tasks/TASK_RESTART',

    POPULATE_TASK : 'tasks/POPULATE_TASK',
    POPULATE_TASK_SUCCESS : 'tasks/POPULATE_TASK_SUCCESS'


}

export const actions = {

    getTasks: (filter = null) => ({ type: types.TASK_GET_ALL_REQUEST, filter}),
    getTasksSuccess : (tasks = { }) => ({ type: types.TASK_GET_ALL_SUCCESS , tasks }),
    getTasksFailure : (error) => ({ type: types.TASK_GET_ALL_FAILURE, error }),

    createTask: (task) => ({ type: types.TASK_ADD,  task }),
    createTaskSuccess : (task, id) => ({ type: types.TASK_ADD_SUCCESS, task,id }),
    createTaskFailure : (error) => ({ type: types.TASK_ADD_FAILURE, error }),

    deleteTask: (task) => ({ type: types.TASK_REMOVE,  task }),
    deleteTaskSuccess : (task) => ({ type: types.TASK_REMOVE_SUCCESS, task }),
    deleteTaskFailure : (error) => ({ type: types.TASK_REMOVE_FAILURE, error }),

    completeTask: (task) => ({ type: types.TASK_COMPLETE,  task }),
    completeTaskSuccess : (task) => ({ type: types.TASK_COMPLETE_SUCCESS, task }),
    completeTaskFailure : (error) => ({ type: types.TASK_REMOVE_FAILURE, error }),

    startTask: (task) => ({ type: types.TASK_START, task }),
    tickTask : (task) => ({ type: types.TASK_TICK, task }),
    stopTask : (task) => ({ type: types.TASK_STOP, task }),
    restartTask : (task) => ({ type: types.TASK_RESTART, task }),

    populateTasks : () => ({ type: types.POPULATE_TASK  }),



}


const taskDefaultProps = {
    id: null,
    description: '',
    duration : 30,
    progress  : 0,
    completed : false,
    started : false,
    created : Date.now(),
    isFetching : false,
    completeDate: null,
}

export class Task extends Record(taskDefaultProps, 'Task') {
    getType() {
        if(this.duration >= 0 && this.duration <= 30){
            return 'corta'
        }
        if(this.duration > 30 && this.duration < 60){
            return 'media'
        }
        return 'larga'
    }
}



const initialState = fromJS({
    tasks : { },
    isFetching : false,
    isPopulating : false,
    filter : null
})

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
      case types.TASK_GET_ALL_SUCCESS:
        const tasks = Object.keys(action.tasks).reduce((prev,curr) => prev.setIn([curr],new Task({id:curr, ...action.tasks[curr] })), new OrderedMap())
        return state.merge({ tasks, filter : action.filter })

      case types.TASK_GET_ALL_REQUEST:
        return state.merge({ isFetching : false })
      case types.TASK_GET_ALL_FAILURE:
          return state.merge({ isFetching : false })

      case types.TASK_ADD:
        return state.set('isFetching', false)

      case types.TASK_ADD_SUCCESS:
        const task = new Task({ ...action.task, id: action.id})
        return state.merge({ isFetching : false }).setIn(['tasks', action.id], task)

      case types.TASK_ADD_FAILURE:
        return state.merge({ isFetching : false })


     case types.TASK_REMOVE_FAILURE:
        return state.merge({ isFetching : false })

     case types.TASK_REMOVE:
        return state.updateIn(['tasks',action.task.id], t => t.set('isFetching',true))

     case types.TASK_REMOVE_SUCCESS:
         return state.set('isFetching',false).deleteIn(['tasks', action.task.id])

     case types.TASK_COMPLETE:
         return state.updateIn(['tasks',action.task.id], t => t.set('isFetching',true))

     case types.TASK_COMPLETE_SUCCESS:
        return state.updateIn(['tasks',action.task.id], t => t.set('isFetching',false).merge(action.task))

     case types.TASK_START:
        return state.setIn(['tasks',action.task.id,'started'], true)

    case types.TASK_STOP:
        if(action.completed)
            return state.updateIn(['tasks',action.task.id], task => task.merge({ started: false, completed : true }))
        return state.setIn(['tasks',action.task.id,'started'], false)

     case types.TASK_TICK:
         return state.updateIn(['tasks',action.task.id, 'progress'], t => t + 1)

     case types.TASK_RESTART:
        return state.updateIn(['tasks',action.task.id, 'progress'], t => 0)
    case types.POPULATE_TASK:
        return state.set('isPopulating',true)
    case types.POPULATE_TASK_SUCCESS:
        return state.set('isPopulating', false)

      default:
        return state
    }
}

export default taskReducer
