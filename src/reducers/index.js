import { persistReducer } from 'redux-persist'
import { combineReducers } from 'redux'
import  tasksReducer from './Tasks'
import storage from 'redux-persist/lib/storage'
import immutableTransform from 'redux-persist-transform-immutable'
import { Task } from '../reducers/Tasks'
import { routerReducer } from 'react-router-redux'



const config = {
  transforms: [immutableTransform({ records: [Task] })],
  key: 'root',
  storage,
  blacklist: ['router'],

}

const reducer = combineReducers({
    tasksReducer,
    router: routerReducer
})

const rootReducer = persistReducer(config, reducer)


export default rootReducer
