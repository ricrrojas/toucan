import axios from 'axios'

export const _dev = process.env.NODE_ENV === 'development'

const BASE_URL = 'https://toucan-21fb3.firebaseio.com/'

const _axios = axios.create({
  baseURL: BASE_URL,
  timeout: 30000,
})

export default _axios
