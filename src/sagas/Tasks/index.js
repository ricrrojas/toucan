import {
  takeLatest,
  put,
  call,
  select
} from 'redux-saga/effects'
import {
  types,
  actions,
} from '../../reducers/Tasks'
import * as API from './api'
import { replace } from 'react-router-redux'
import { createRandomTask } from '../../utils/TaskUtils'

function * fetchTasks({ filter }) {
    try {
        const tasks = yield call(API.getTasks, filter)
        yield put(actions.getTasksSuccess(tasks || { }))
    } catch (e) {
      yield put(actions.getTasksFailure(e))
    }
}

function * createTask({ task }){
  try {
      const result = yield call(API.postTask, task)
      yield put(actions.createTaskSuccess(task, result.name))
      yield put(replace('/'))
  } catch (e) {
    yield put(actions.createTaskFailure(task ))
  }
}

function * removeTask({ task }){
  try {
      yield call(API.deleteTask, task.id)
      yield put(actions.deleteTaskSuccess(task))
  } catch (e) {
      yield put(actions.deleteTaskFailure(e))
  }
}

function * completeTask({ task }){
  try {
      task = task.set('completed',true).set('completeDate', Date.now())
      yield call(API.putTask, task)
      yield put(actions.completeTaskSuccess(task))
    } catch (e) {
      yield put(actions.completeTaskFailure(e))
   }
}

function * onTickTask({ task }){
    const _task = yield select((state) => state.tasksReducer.getIn(['tasks', task.id]))
    if(_task.progress >= task.duration){
      yield put(stopTask(task, true))
    }
}

function * generateInitialTask() {
    const noTasks = 50
    for(let index = 0; index < noTasks;index++){
      const task = createRandomTask(`tarea_${index}`)
      try{
        const result = yield call(API.postTask, task)
        yield put(actions.createTaskSuccess(task, result.name))
      }catch (e) { }
    }
}

let timer = null;


export const startTask = (task) => (dispatch) => {
  clearInterval(timer);
  timer = setInterval(() => dispatch(tick(task)), 1000);
  dispatch({ type: types.TASK_START, task, });
  dispatch(tick(task))
}


export const tick = (task) => ({ type: types.TASK_TICK, task });

export const stopTask = (task, completed) => {
  clearInterval(timer);
  return { type: types.TASK_STOP, task, completed };
}


function * completeTaskSaga() {
  yield takeLatest(types.TASK_COMPLETE, completeTask)
}

function * getTasksSaga() {
  yield takeLatest(types.TASK_GET_ALL_REQUEST, fetchTasks)
}

function * createTaskSaga() {
  yield takeLatest(types.TASK_ADD, createTask)
}

function * removeTaskSagas() {
  yield takeLatest(types.TASK_REMOVE, removeTask)
}

function *  onTickTaskSaga(){
  yield takeLatest(types.TASK_TICK, onTickTask)
}


function * generateInitialTaskSaga(){
  yield takeLatest(types.POPULATE_TASK, generateInitialTask)
}


export default [
    getTasksSaga,
    createTaskSaga,
    removeTaskSagas,
    completeTaskSaga,
    onTickTaskSaga,
    generateInitialTaskSaga
]
