import api from '../api'

export const getTasks = () => {
    return api.get('/tasks.json').then(res => res.data)
}

export const postTask = (task) => {
    return api.post(`/tasks.json`, task).then(res => res.data)
}

export const putTask = (task) => {
    return api.put(`/tasks/${task.id}.json`, task).then(res => res.data)
}

export const deleteTask = (id) => {
    return api.delete(`/tasks/${id}.json`).then(res => res.data)
}
