import React from 'react'
import ReactDOM from 'react-dom'
import configureStore from './store/configureStore'
import createHistory from 'history/createBrowserHistory'
import App  from './containers/App'


const history = createHistory()
const store = configureStore(history)


ReactDOM.render(<App  store={store} history={history}/>, document.getElementById('root'))
